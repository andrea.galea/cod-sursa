#include<LiquidCrystal.h>
#include<Servo.h>
#define LDR A0
#define LED1 A1  //FAZA LUNGA
#define LED2 A2 //FAZA SCURTA
#define BUTON 8


const int trig=13;
const int echo=12;


const float K = 0.5; 
int ValLdr=0,ValButon=0; 

const int rs=2, en=3, d4=4, d5=5, d6=6, d7=7;
LiquidCrystal lcd (rs, en, d4, d5, d6, d7);

void setup() {
  Serial.begin(9600);
  lcd.begin(16, 2);

  pinMode(echo, INPUT);
  pinMode(trig, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LDR, INPUT);
  pinMode(BUTON, INPUT); 

}

void loop() {

  int pragLDR=MapareLDR();

  if(pragLDR<150){
    Ultrasonic(); 
    delay(10);
  }else{
    digitalWrite(LED2, HIGH);
    delay(1);
    ApasareButon();
    delay(10);
  }
  

}
void Ultrasonic(){
  float pragHC= FiltrareUltrasonic();
  
  if(pragHC<5)
  {
    digitalWrite(LED1,LOW);
    digitalWrite(LED2,HIGH);
    lcd.clear();
    afisareOFF();
     delay(3);
  }
  else
  {
    digitalWrite(LED1,HIGH);
    digitalWrite(LED2,HIGH);
    
    delay(3);
    lcd.clear();
    afisareON();
    delay(3);
  }
}

float FiltrareUltrasonic() {
  float distantaNoua =0.0;
  float distantaVeche=0.0;

  digitalWrite(trig, LOW);
  delayMicroseconds(2);
  digitalWrite(trig, HIGH);
  delayMicroseconds(10);
  digitalWrite(trig, LOW);

  long timp = pulseIn(echo, HIGH);
  float distanta = (timp * 0.0343) / 2;  
  distantaNoua= (K * distanta) + (1 - K) * distantaVeche;
  distantaVeche =distantaNoua;

 
 return distantaNoua;
}

int MapareLDR(){
  ValLdr=analogRead(LDR);
  ValLdr=map(ValLdr, 0, 1023, 0, 255);
  return ValLdr;
}

void ApasareButon(){
  ValButon=digitalRead(BUTON);
  
  if(ValButon == HIGH)
  {
    digitalWrite(LED1,HIGH);
    lcd.clear();
    afisareON();
    delay(3);
  }else{
    digitalWrite(LED1,LOW);
    lcd.clear();
    afisareOFF();
    delay(3);
  }
}
void afisareON(){
    lcd.setCursor(0, 0); 
    lcd.print("Faza lunga: ON"); 
}

void afisareOFF(){
    lcd.setCursor(0, 0); 
    lcd.print("Faza lunga: OFF"); 
}